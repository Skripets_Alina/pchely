$(function() {
    /*calculate browser width*/
    var browser_width=window.outerWidth-10;
    console.log('Browser width=' + browser_width + 'px');

	var $red = '#f35f5f';
	var $yellow = '#fbfd46';

    var width = $(window).width();
    $('.fa').click(function() {
        $('li').find('ul').css('display','none');
        if ($(this).parent().find('ul').hasClass('clicked')) {
            $(this).parent().find('ul').css('display','none');
            $(this).parent().find('ul').removeClass('clicked');

        }
        else {
            $(this).parent().find('ul').addClass('clicked');
            $(this).parent().find('ul').css('display','block');

        }
    });

	/*hamburger menu animation:
	transforming to cross after click
	and back to the hamburger menu*/
	var Menu = {
        el: {
            ham: $('.hamburger'),
            menuTop: $('.menu-top'),
            menuMiddle: $('.menu-middle'),
            menuBottom: $('.menu-bottom')
        },
        init: function() {
            Menu.bindUIactions();
        },
        bindUIactions: function() {
            Menu.el.ham
                .on(
                  'click',
                    function(event) {
                        Menu.activateMenu(event);
                        event.preventDefault();
                    } 
                );
        },
        activateMenu: function() {
            Menu.el.menuTop.toggleClass('menu-top-click');
            Menu.el.menuMiddle.toggleClass('menu-middle-click');
            Menu.el.menuBottom.toggleClass('menu-bottom-click'); 
        }
    };
    Menu.init();

    /*navigation menu animation: appearign from left
    and hidding after click on hamburger menu*/
    var FF = !(window.mozInnerScreenX == null);
    $('.hamburger').click(function() {
        if(FF) {
            if  ($('span').hasClass('menu-middle-click')) {
                $('.inner').css('left','0');
                $('.menu-global').css('display','none');
                $('.hamburger').css('background',"url('img/hamburger_cross.png')");
                $('.hamburger').css('background-size','contain');
                $('.hamburger').css('background-repeat','no-repeat');
            }
            else {
                $('.inner').css('left','-400px');
                $('.hamburger').css('background',"url('img/hamburger.png')");
                $('.hamburger').css('background-size','contain');
                $('.hamburger').css('background-repeat','no-repeat');
            }
        }
        else { 
    	    if ($('span').hasClass('menu-middle-click')) {
    	    	$('.inner').css('left','0');
    	    }
    	    else {
    	    	$('.inner').css('left','-400px');
            }
        }
    });
});


