$(function() {
    $('.adress').click(function(){
        if ($('.adress').hasClass('opened')) {
            $('.adress').removeClass('opened');
            $('.adress').addClass('closed');
            $('.hidden_adress').css('opacity','0');

        }
        else {
            $('.adress').removeClass('closed');
            $('.adress').addClass('opened');
            $('.hidden_adress').css('opacity','1');
        }        

    });

    $(function() {
  $('a[href=#header]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });

 });
});